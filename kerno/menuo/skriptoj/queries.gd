extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "rajtigo"


# URL к API
const URL_DATA = "https://t34.tehnokom.su/api/v1.1/"


# запрос на список управляемых объектов в космосе
func get_direktebla_kosmo_query():
	var query = JSON.print({ "query": "query ($UzantoId:Int )"+
	# "{ universoObjekto (" +
	"{ filteredUniversoObjekto (" +
	# " universoObjektojUniversoobjektoligiloLigiloObjekto_Isnull:true," +
	" universoobjektouzanto_Isnull:false,"+
	" universoobjektouzanto_Autoro_SiriusoUzanto_Id:$UzantoId," +
	" koordinatoX_Isnull:false, koordinatoY_Isnull:false, koordinatoZ_Isnull:false," +
	" kubo_Isnull:false," +
	") { edges { node { uuid " +
	"	realeco{objId}}}}}",
	'variables': {"UzantoId":Global.id} })
#	print("query 2 = ",query)
	return query


# задаём координаты и угол поворота объекту, удаляем связь по нахождению внутри
func go_objekt_kosmo_query(uuid, koordX, koordY, koordZ, rotaciaX, rotaciaY, rotaciaZ, uuid_ligilo_del, kuboId = 1):
	var del = ''
	if uuid_ligilo_del:
		del = ' redaktuUniversoObjektoLigiloj (uuid: "'+uuid_ligilo_del+'",'
		del = del + 'forigo:true) {status message universoObjektojLigiloj{ uuid } } '

	var query = JSON.print({ 'query': 'mutation ($uuid:UUID, $koordX:Float, $koordY:Float, $koordZ:Float,'+
		' $rotaciaX:Float, $rotaciaY:Float, $rotaciaZ:Float, $kuboId:Int )'+
		' { redaktuUniversoObjekto ( uuid: $uuid, koordinatoX: $koordX, koordinatoY: $koordY, '+
		' koordinatoZ: $koordZ, rotaciaX: $rotaciaX, rotaciaY: $rotaciaY, rotaciaZ: $rotaciaZ, '+
		' kuboId: $kuboId) '+
		' { status message universoObjektoj { uuid } } ' + del + '}',
		'variables': {"uuid":uuid, "koordX": koordX, "koordY": koordY, "koordZ": koordZ,
		"rotaciaX": rotaciaX, "rotaciaY": rotaciaY, "rotaciaZ": rotaciaZ, 
		"kuboId":kuboId } })
	# print('==go_objekt_kosmo_query == ', query)
	return query


# завершение задачи, проекта, запись в таблицу связей, что находимся внутри другого объекта
# station_uuid - в какой станции находимся
func eniri_kosmostacio(projekto_uuid, tasko_uuid, station_uuid):
	var statusoId = 4
	var tipoId = 3 #находится внутри
	var param = ''
	var mutateProjekto = ''

	if projekto_uuid:
		param = ', $tasko_uuid:UUID, $projekto_uuid:UUID, $statusoId:Int'
		mutateProjekto = 'redaktuUniversoTaskoj (uuid: $tasko_uuid, statusoId:$statusoId) { status '
		mutateProjekto = mutateProjekto + ' message universoTaskoj { uuid } } '
		mutateProjekto = mutateProjekto + 'redaktuUniversoProjekto (uuid: $projekto_uuid,  '
		mutateProjekto = mutateProjekto + ' statusoId:$statusoId) { status '
		mutateProjekto = mutateProjekto + ' message universoProjekto { uuid } } '
	return JSON.print({ 'query': 'mutation ($posedantoUuid:String, $ligiloUuid:String, '+
		' $tipoId:Int '+param+')'+
		'{ '+
		' redaktuUniversoObjektoLigiloj ( posedantoUuid:$posedantoUuid,' +
		'  ligiloUuid:$ligiloUuid, tipoId:$tipoId) { message status' +
		'  universoObjektojLigiloj{ uuid } }' +
		mutateProjekto +
		'}',
		'variables': {"tasko_uuid":tasko_uuid, "statusoId": statusoId, "projekto_uuid":projekto_uuid,
			"posedantoUuid":station_uuid, 
			"ligiloUuid":Global.direktebla_objekto[Global.realeco-2]['uuid'], 
			"tipoId":tipoId } })

