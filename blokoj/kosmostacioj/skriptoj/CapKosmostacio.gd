extends Spatial

const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")

func _on_kosmo_pressed():
	if not Global.direktebla_objekto[Global.realeco-2].has('uuid'):
		print('Нет корабля для этого мира')
		return
	var q = QueryObject.new()
	# Делаем запрос к бэкэнду для получения списка управляемых объектов.
	# Ответ будет обрабатываться в функции get_direktebla_request_complete
	var del_uuid = '' #есть ли в базе запись о нахождении в станции
	var kuboId = 1 #в каком кубе находится станция
	# задаём координаты выхода из станции, согласно координатам станции
	if len(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'])>0:
		Global.direktebla_objekto[Global.realeco-2]['koordinatoX'] = \
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoX'] + 120
		Global.direktebla_objekto[Global.realeco-2]['koordinatoY'] = \
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoY'] + 120
		Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'] = \
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoZ'] + 200
		del_uuid = Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['uuid']
		kuboId = Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['kubo']['objId']
		Global.direktebla_objekto[Global.realeco-2]['rotaciaX'] = 0
		Global.direktebla_objekto[Global.realeco-2]['rotaciaY'] = 0
		Global.direktebla_objekto[Global.realeco-2]['rotaciaZ'] = 0
	Global.direktebla_objekto[Global.realeco-2]['kosmo'] = true
	
	# Разрегистрируем обработчик сигнала request_completed (вызывается
	# по завершении HTTPRequest)
	Title.get_node("request").connect('request_completed', Title, 'komenci_request_complete')

	var error = Title.get_node("request").request(q.URL_DATA, 
		Global.backend_headers, true, 2, 
		q.go_objekt_kosmo_query(
			Global.direktebla_objekto[Global.realeco-2]['uuid'],
			Global.direktebla_objekto[Global.realeco-2]['koordinatoX'],
			Global.direktebla_objekto[Global.realeco-2]['koordinatoY'], 
			Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'],
			0,0,0, del_uuid, kuboId
		))
#	go_objekt_kosmo_query(uuid, koordX, koordY, koordZ, rotaciaX, rotaciaY, rotaciaZ, uuid_ligilo_del, kuboId = 1):	
	get_tree().change_scene('res://blokoj/kosmo/scenoj/space.tscn')

	# Если запрос не выполнен из-за какой-то ошибки
	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
	if error != OK:
		print('Error in GET (direktebla) Request.')


const base_ship = preload("res://blokoj/kosmosxipoj/scenoj/base_ship.tscn")
const base_cabine = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/cabine.tscn")
const base_engines = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/engines.tscn")
const base_cargo = preload("res://blokoj/kosmosxipoj/scenoj/moduloj/cargo.tscn")

func _on_CapKosmostacio_ready():
	# устанавливаем корабль на место 1А
	var ship = null
	var cabine = null
	var cargo = null
	var engines = null
	ship = base_ship.instance()
	engines = base_engines.instance()
	ship.get_node("CollisionShape").add_child(engines)
	cabine = base_cabine.instance()
	ship.get_node("CollisionShape").add_child(cabine)
	cargo = base_cargo.instance()
	ship.get_node("CollisionShape").add_child(cargo)
	ship.visible=true
	ship.rotate_y(1.58)
	ship.translation.x = ship.translation.x + 4.2 # насколько въезжать в парковку
	ship.translation.z = ship.translation.z - 5 # в сторону от центра
	ship.translation.y = ship.translation.y - 3 # высота от пола
	add_child(ship,true)




