extends Node2D


onready var status = get_node("ParallaxBackground/Status")


const QueryObject = preload("res://blokoj/kosmostacioj/skriptoj/queries.gd")


func _ready():
	$'ParallaxBackground/GreetingLabel'.text = "Приветствую %s!" % Global.nickname
	Net.connect("connection_failed", self, "_on_connection_failed")
	Net.connect("connection_succeeded", self, "_on_connection_success")
	Net.connect("server_disconnected", self, "_on_server_disconnect")
	Net.connect("players_updated", self, "update_players_list")
	Net.my_name = Global.login
	Net.connect_to_server()

	# Создаём объект с данными для запросов.
	# Данные для запросов и сами запросы храним в queries.gd
	var q = QueryObject.new()
	# Делаем запрос к бэкэнду для получения списка управляемых объектов.
	# Ответ будет обрабатываться в функции get_direktebla_request_complete
	var error = Title.get_node("request").request(q.URL, Global.backend_headers, true, 2, 
		q.get_direktebla_query(2, 3, 2))
	
	# Если запрос не выполнен из-за какой-то ошибки
	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
	if error != OK:
		print('Error in GET (direktebla) Request.')



# Обработчик сигнала "connection_succeeded"
func _on_connection_success():
	status.text = "Connected"
	status.modulate = Color.green


# Обработчик сигнала "connection_failed"
func _on_connection_failed():
	status.text = "Connection Failed, trying again"
	status.modulate = Color.red


# Обработчик сигнала "server_disconnected"
func _on_server_disconnect():
	status.text = "Server Disconnected, trying to connect..."
	status.modulate = Color.red
	

# Обработчик сигнала "players_updated"
func update_players_list():
	pass
