extends HTTPRequest


# Обработчик ответа на HTTP запрос к бэкэнду
func _on_HTTPRequestFind_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	var simpled_data = parsed_resp['data']['filteredUniversoObjekto']['edges']
	
#	$'../'.ItemListContent.clear()
	$'../canvas/MarginContainer/VBoxContainer/scroll/ItemList'.clear()
	Global.objektoj.clear()
	var i = 0
	for item in simpled_data:
		
		if item['node']['posedantoId'] == Global.id:
			# обновляем данные directable по своему кораблю
			var kosmo = Global.direktebla_objekto[Global.realeco-2]['kosmo']
			print('==kosmo==',kosmo)
			Global.direktebla_objekto[Global.realeco-2]=item['node']
			Global.direktebla_objekto[Global.realeco-2]['kosmo'] = kosmo
			pass
		else:# свой корабль не добавляем в список
			Global.objektoj.append(item['node'])
			Global.objektoj[i]['distance'] = 0
			i += 1
#			var itemj ={
#				'nomo':item['node']['nomo']['enhavo'],
#				'priskribo':item['node']['priskribo']['enhavo'],
#				'uuid':item['node']['uuid'],
#				'koordinatoX':item['node']['koordinatoX'],
#				'koordinatoY':item['node']['koordinatoY'],
#				'koordinatoZ':item['node']['koordinatoZ'],
#				'posedantoId':item['node']['posedantoId'],
#				'posedantoObjekto':item['node']['posedantoObjekto'],
#				'distance':0,
#			}
#			$'../'.ItemListContent.append(itemj)
	$'../'.FillItemList()
	if $"/root".get_node_or_null('space'):
		$"/root".get_node('space').emit_signal("load_objektoj")
