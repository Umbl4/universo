extends Control


var itineroj = []
#uuid_tasko - uuid задачи, когда уже летим
#uuid - uuid цели полёта, если это объект 
#nomo - название объекта цели 
# координаты цели полёта
#			'koordinatoX':
#			'koordinatoY':
#			'koordinatoZ':
# расстояние до цели полёта - distance

func _on_Close_button_pressed():
	$"canvas/MarginContainer".set_visible(false)


func _resize(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$canvas/MarginContainer.rect_size += event.relative


func _drag(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$canvas/MarginContainer.rect_position += event.relative


func FillItemList():
	$"canvas/MarginContainer/VBoxContainer/ItemList".clear()
	# Заполняет список найдеными продуктами
	for Item in itineroj:
		get_node("canvas/MarginContainer/VBoxContainer/ItemList").add_item('('+String(int(Item['distance']))+') '+Item['nomo'], null, true)

#пересчет дистанции до объектов в списке
func distance_to(trans):
	for obj in itineroj:
		obj['distance'] = trans.distance_to(Vector3(obj['koordinatoX'],
			obj['koordinatoY'],obj['koordinatoZ']))
	$'canvas/MarginContainer/VBoxContainer/ItemList'.clear()
#	$'canvas/MarginContainer/VBoxContainer/scroll/ItemList'.clear()
	FillItemList()


#отправляем корабль в полёт
func go_ship():
	if len(itineroj)==0:
		return 404
	var position = Vector3(itineroj[0]['koordinatoX'],itineroj[0]['koordinatoY'],itineroj[0]['koordinatoZ'])
	$"../../ship".set_way_point(position,null)
	$"../../way_point".set_way_point(position)

func komenci_itinero():
	$"../../ship".vojkomenco()#начинаем движение
	# отправляем в полёт
	go_ship()
	#запускаем таймер
	$"../../timer".start()
	$canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero.disabled=true


func _on_kom_itinero_pressed():
	#остановить текущие задачи
	# останавливаем таймер передачи данных на сервер
	$"../../timer".stop()
	#отправка последних координат и закрытие задачи с проектом
	$"../../ship".finofara_flugo()
	#создать проект
	#создать список задач на основе списка itineroj
	komenci_itinero()


func add_itinero(uuid_tasko, uuid, nomo, koordX, koordY, koordZ, distance):
	itineroj.append({
		'uuid_tasko':uuid_tasko,
		'uuid':uuid,
		'nomo':nomo,
		'koordinatoX':koordX,
		'koordinatoY':koordY,
		'koordinatoZ':koordZ,
		'distance':distance,
	})
	FillItemList()

func clear_itinero():
	itineroj.clear()
	FillItemList()

func _on_itinero_fin_pressed():
	#если в полёте
	if $canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero.disabled:
		if $canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin.text=='Пауза':
			#останавливаем движение корабля
			$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin.text='Далее'
			$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next.disabled=true
			$"../../ship".clear_way_point()
			$"../../way_point".set_active(false)
		else:
			$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin.text='Пауза'
			#продолжаем движение корабля
			$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next.disabled=false
			go_ship()
			# itinero_clear

func _on_itinero_clear_pressed():
	if len(itineroj)==0:
		return 404
	#закрыть все задачи и проект
	#удаляем все задачи далее первой
	itineroj = [itineroj[0],]
	#закрываем текущую задачу и проект автоматически закрывается
	$"../../ship".clear_way_point()
	$"../../way_point".set_active(false)
	$"../../timer".stop()
	$"../../ship".finofara_flugo()
#	itineroj.clear()
#	$canvas/MarginContainer/VBoxContainer/ItemList.clear()
	$canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero.disabled=false
	$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next.disabled=true
	$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin.disabled=true
	$canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_clear.disabled=true
	pass # Replace with function body.


# пропустить текущую цель и лететь к следующей
func _on_itinero_next_pressed():
	$"../../ship".clear_way_point()
	$"../../way_point".set_active(false)
	$"../../timer".stop()
	$"../../ship".finofara_flugo()
	pass # Replace with function body.
