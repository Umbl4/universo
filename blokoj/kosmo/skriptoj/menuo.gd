extends Control

onready var margin = $"/root/Title/CanvasLayer/UI/L_VBox/Objektoj/Window/canvas/MarginContainer"
onready var ship = $"../ship"

var index_pos = 0

func _input(event: InputEvent) -> void:
	
	if Input.is_action_just_pressed("right_click"):
#		event.pressed=true
		if event is InputEventMouseButton and $"/root/Title/CanvasLayer/UI/L_VBox/Objektoj/Window/canvas/MarginContainer".visible:
			$canvas/PopupMenu.set_item_disabled(2,true)
#			$canvas/PopupMenu.mouse_filter=2
			var x = $".".get_global_mouse_position().x
			var y = $".".get_global_mouse_position().y
			if (margin.margin_top<y) and (y<margin.margin_bottom) and (margin.margin_left<x) and (x<margin.margin_right):
				$canvas/PopupMenu.margin_left=x
				$canvas/PopupMenu.margin_top=y
				$canvas/PopupMenu.visible=true
#				#если пункт меню - станция
				x = $"/root/Title/CanvasLayer/UI/L_VBox/Objektoj/Window/canvas/MarginContainer/VBoxContainer/scroll/ItemList".get_local_mouse_position().x
				y = $"/root/Title/CanvasLayer/UI/L_VBox/Objektoj/Window/canvas/MarginContainer/VBoxContainer/scroll/ItemList".get_local_mouse_position().y
				index_pos = $"/root/Title/CanvasLayer/UI/L_VBox/Objektoj/Window/canvas/MarginContainer/VBoxContainer/scroll/ItemList".get_item_at_position(Vector2(x,y),true)
				$"/root/Title/CanvasLayer/UI/L_VBox/Objektoj/Window/canvas/MarginContainer/VBoxContainer/scroll/ItemList".select(index_pos)
				if Global.objektoj[index_pos]['resurso']['objId'] == 1:#объект станция Espero
					#проверяем как далеко от станции и если менее 20, то разрешаем войти
					var dist = $"../ship".translation.distance_to(Vector3(
						Global.objektoj[index_pos]['koordinatoX'],
						Global.objektoj[index_pos]['koordinatoY'],
						Global.objektoj[index_pos]['koordinatoZ']
					))
					if dist<400:
						$canvas/PopupMenu.set_item_disabled(2,false)
				

# сдвиг по всем координатам по целеполаганию полёта к объекту
const translacio = 20
const translacio_stat = 300

const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")

func _on_PopupMenu_index_pressed(index):
	if index == 2:
		# Разрегистрируем обработчик сигнала request_completed (вызывается
		# по завершении HTTPRequest)
		Title.get_node("request").connect('request_completed', Title, '_on_eniri_kosmostacio_request_completed')
		var q = QueryObject.new()
		# закрываем проект
		#  добавляем запись в связи, что находимся внутри
		var uuid_tasko = ''
		if $"../ship".projekto_uuid:
			uuid_tasko = $"../b_itinero/itinero".itineroj[0]['uuid_tasko']
		var error = Title.get_node("request").request(q.URL_DATA, 
			Global.backend_headers,
			true, 2, q.eniri_kosmostacio(
				$"../ship".projekto_uuid,
				uuid_tasko, 
				Global.objektoj[index_pos]['uuid']))
		# Если запрос не выполнен из-за какой-то ошибки
		# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
		if error != OK:
			print('Error in GET (_on_eniri_kosmostacio_request_completed) Request.')
		Title.CloseWindow()
		Global.direktebla_objekto[Global.realeco-2]['kosmo'] = false
		# вызываем сцену станции
		get_tree().change_scene('res://blokoj/kosmostacioj/CapKosmostacio.tscn')

	else:
		# вычисляем точку в пространстве, придвинутую на translacio ближе
		# если станция, то дистанция больше
		var celo = Vector3(Global.objektoj[index_pos]['koordinatoX'],
			Global.objektoj[index_pos]['koordinatoY'],
			Global.objektoj[index_pos]['koordinatoZ'])
		
		var dist = 0
		if Global.objektoj[index_pos]['resurso']['objId']==1:
			dist = $"../ship".translation.distance_to(celo) - translacio_stat
		else:
			dist = $"../ship".translation.distance_to(celo) - translacio
		var speed = celo - $"../ship".translation
		celo = $"../ship".translation + speed.normalized() * dist

		$"../b_itinero/itinero".add_itinero(
			'',
			Global.objektoj[index_pos]['uuid'],
			Global.objektoj[index_pos]['nomo']['enhavo'], #'nomo'
			celo.x,
			celo.y,
			celo.z,
			dist
		)
		$"../b_itinero/itinero".FillItemList()
		if index==0:
			$"../b_itinero/itinero".komenci_itinero()
	#	elif index==1:


func _on_PopupMenu_draw():
#	$canvas/PopupMenu.mouse_filter=0 #для отключения игнорирования меню и его реакции на выбор пунктов
	pass # Replace with function body.

